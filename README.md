# Test Data

Source PCAPs are stored in the `data` subdirectory, and a container PCAP was created using:

> mergecap -F pcap -a deencap_context_not_init.pcap deencap_invalid_label.pcap deencap_too_much_data.pcap encap_frag_bridged.pcap encap_frag_id_error.pcap add_ext_frag_ext2.pcap deencap_bad_protocol.pcap deencap_complete_label3.pcap deencap_incomplete_pdu.pcap encap_frag_crc_error.pcap encap_frag.pcap deencap_bad_total_length.pcap deencap_padding.pcap -w gse_datafield.pcap

Most of these files contain erroneous input, and can be used when developing GSE de-encapsulators, and for (non-)regression tests.

The source PCAP files were from taken from `libgse`, (see: `https://forge.net4sat.org/opensand/libgse`) though they've been slightly modified (MAC addresses and EtherTypes).
